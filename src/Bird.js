import React from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import birdData from './bird.json';

const Bird =()=>{

    return(
        <div>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                    <TableHead>
                    <TableRow sx={{backgroundColor: "lightGray"}}>
                        <TableCell align="left" sx={{ fontWeight: "bold" }}>Finnish</TableCell>
                        <TableCell align="left" sx={{ fontWeight: "bold" }}>Swedish</TableCell>
                        <TableCell align="left" sx={{ fontWeight: "bold" }}>English</TableCell>
                        <TableCell align="left" sx={{ fontWeight: "bold" }}>Short</TableCell>
                        <TableCell align="left" sx={{ fontWeight: "bold" }}>Latin</TableCell>
                    </TableRow>
                    </TableHead>
                    <TableBody>
                    {birdData.sort((a,b) => {
                        return a.finnish.localeCompare(b.finnish)}).map((name) => (
                            <TableRow
                            key={name.short}
                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                            >
                            <TableCell component="th" scope="row">
                                {name.finnish}
                            </TableCell>
                            <TableCell align="left">{name.swedish}</TableCell>
                            <TableCell align="left">{name.english}</TableCell>
                            <TableCell align="left">{name.short}</TableCell>
                            <TableCell align="left">{name.latin}</TableCell>
                            </TableRow>
                    ))}
                    </TableBody>
                </Table>
                </TableContainer>
        </div>
    )
}

export default Bird;